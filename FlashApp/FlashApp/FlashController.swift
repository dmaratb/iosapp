//
//  ViewController.swift
//  FlashApp
//
//  Created by Marat on 04/11/2020.
//

import UIKit
import AVFoundation

class FlashController: UIViewController {

    var flashCallback : (([Destination])->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }


    // MARK: - Torch
    func playSequence(_ originalSequence:[Int], forLength length:Double, completionHandler: @escaping(Int) -> Void) -> Void {

        var sequence = originalSequence
        guard let device = AVCaptureDevice.default(for: .video) else {
            completionHandler(-1)
            return
        }

        guard device.hasTorch else {
            completionHandler(-2)
            return
        }

        Timer(timeInterval: length, repeats: true) { (timer) in

            let signal = sequence.removeLast()

            if signal > 0 {
                self.turnTorchOn(device)
            } else {
                self.turnTorchOff(device)
            }

            if sequence.count < 1 {
                timer.invalidate()
                completionHandler(1)
            }
        }.fire()

        return
    }

    private func turnTorchOn(_ device: AVCaptureDevice) -> Void {
        do {
            try device.setTorchModeOn(level: 1)
        } catch {
            // error
        }
    }

    private func turnTorchOff(_ device: AVCaptureDevice) -> Void {
        device.torchMode = .off
    }

    // MARK: - Network
    func getSequence(completionHandler: @escaping(Int, [Int]) -> Void) -> Void {

        let lat = 31.95892
        let lon = 34.87995
        let initUrlStr = "http://192.168.0.100:8000/myway/?action=init&lat=\(lat)&lon=\(lon)"

        let url = URL(string: initUrlStr)

        guard let requestUrl = url else {
            completionHandler(-1, [])
            return
        }

        var request = URLRequest(url: requestUrl)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                completionHandler(-2, [])
            }

            if let innerData = data {
                do {
                    let dataResponse = try JSONDecoder().decode(InitResponse.self, from: innerData)
                    var sequence:[Int] = []
                    switch dataResponse.status {
                        case .OK:
                            for nextChar in dataResponse.data.sequence.split(separator: ",") {
                                switch nextChar {
                                    case "0":
                                        sequence.append(0)
                                        break;
                                    case "1":
                                        sequence.append(1)
                                        break
                                    default:
                                        // broken
                                        break
                                }
                            }
                            completionHandler(1, sequence)
                            break

                        case .ERROR:
                            completionHandler(-4, sequence)
                            break

                        default:
                            break
                    }
                    return

                } catch {
                    completionHandler(-3, [])
                    return
                }
            }
        }

        task.resume()
    }


    func getDestinations(callback: @escaping(Status, [Destination]) -> Void) -> Void {

        let initUrlStr = "http://192.168.0.100:8000/myway/?action=init_result"
        let url = URL(string: initUrlStr)

        guard let requestUrl = url else {
            callback(.ERROR, [])
            return
        }

        var request = URLRequest(url: requestUrl)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                callback(.ERROR, [])
                return
            }

            if let innerData = data {
                do {
                    let dataResponse = try JSONDecoder().decode(DestinationResponse.self, from: innerData)
                    callback(dataResponse.status, dataResponse.data)
                    return
                } catch {
                    callback(.ERROR, [])
                    return
                }
            }
        }

        task.resume()
    }

    // MARK: - Actions
    @IBAction func startAct(_ sender: UIButton) {

        let toneLength = 0.2
        getSequence { (code, sequence) in

            if code > 0 {

                let completionHandler:((Int)->Void) = { code in
                    if code > 0 {
                        // start polling destinations
                        Timer(timeInterval: 1, repeats: true) { (timer) in

                            self.getDestinations { (code, destinations) in

                                switch code {
                                    case .OK:
                                        timer.invalidate()
                                        self.flashCallback?(destinations)
                                        DispatchQueue.main.async { self.navigationController?.popViewController(animated: true) }
                                        break
                                    case .ERROR:
                                        timer.invalidate()
                                        break
                                    case .INPROGRESS:
                                        // keep polling
                                        break
                                    default:
                                        print("error")
                                }
                            }
                        }.fire()
                        
                    } else {
                        //
                    }
                }
                self.playSequence(sequence, forLength: toneLength, completionHandler: completionHandler)
                
            } else {
                //
            }
        }
    }


}
