//
//  DestinationCell.swift
//  FlashApp
//
//  Created by Marat on 05/11/2020.
//

import UIKit

class DestinationCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
