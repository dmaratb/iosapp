//
//  ApiResponses.swift
//  FlashApp
//
//  Created by Marat on 23/11/2020.
//

import Foundation

protocol BaseResponse: Codable {
    var status: Status { get }
    var message: String { get }
}

struct InitResponse: BaseResponse {
    let status: Status
    let message: String
    let data: InitResponseData
}

struct InitResponseData: Codable {
    let sequence: String
}

struct DestinationResponse: BaseResponse {
    let status: Status
    let message: String
    let data: [Destination]
}

struct Destination: Codable {
    let code:Int
    let name:String
}

struct DirectionResponse: BaseResponse {
    let status: Status
    let message: String
    let data: DirectionResponseData
}

struct DirectionResponseData:Codable {
    let azimuth:Float
    let distance:Int
}

enum Status: Int, Codable {
    case OK = 1
    case ERROR = 2
    case INPROGRESS = 3
}
