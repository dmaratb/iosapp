//
//  NavigationController.swift
//  FlashApp
//
//  Created by Marat on 06/11/2020.
//

import UIKit
import CoreLocation
import CoreMotion

class NavigationController: UIViewController, CLLocationManagerDelegate {
    // MARK: - Views

    @IBOutlet weak var horizontalPopup: UIView!
    @IBOutlet weak var arrowView: UIImageView!

    
    // MARK: - Data
    var destination:Destination?
    var angle = CGFloat(0)
    var pause = false


    let locationManager: CLLocationManager = {
        $0.requestWhenInUseAuthorization()
        $0.startUpdatingHeading()
        return $0
    }(CLLocationManager())
    let motionManager = CMMotionManager()


    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locationManager.delegate = self

        if motionManager.isDeviceMotionAvailable {
            // start updating
            motionManager.deviceMotionUpdateInterval = 0.1
            let queue = OperationQueue()

            motionManager.startDeviceMotionUpdates(to: queue, withHandler: { (motionData, error) in
                if let attitude = motionData?.attitude {

                    if abs(attitude.pitch) > 1 {
                        // stop
                        self.pause = true
                        self.horizontalPopup.isHidden = false
                        self.locationManager.stopUpdatingHeading()
                    } else {
                        // continue
                        self.horizontalPopup.isHidden = true
                        self.locationManager.startUpdatingHeading()
                    }
                }
            })

        } else {
            // hz
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let code = destination?.code {
            startUpdatingDestination(code)
        }
    }

    // MARK: - Network
    func startUpdatingDestination(_ code: Int) {

        if code > 0 {
            Timer(timeInterval: 1, repeats: true) { (timer) in

                let getDirectionUrlStr = "http://192.168.0.100:8000/myway/?action=get&destination=\(code)"

                let url = URL(string: getDirectionUrlStr)

                guard let requestUrl = url else {
                    return
                }

                var request = URLRequest(url: requestUrl)
                request.httpMethod = "GET"

                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if error != nil {

                    }

                    if let innerData = data {
                        do {
                            let dataResponse = try JSONDecoder().decode(DirectionResponse.self, from: innerData)

                            switch dataResponse.status {
                                case .OK:
                                    let data = dataResponse.data
                                    if data.distance > 0 {
                                        self.angle = CGFloat(data.azimuth)
                                    } else {
                                        // show popup - arrived
                                        timer.invalidate()
                                    }
                                    break
                                case .INPROGRESS: // skip
                                    break
                                case .ERROR:
                                    // show popup - error
                                    timer.invalidate()
                                    break
                                default:
                                    break
                            }

                        } catch {
                            // server error
                            return
                        }
                    }
                }

                task.resume()

            }.fire()
        }

        //
    }


    // MARK: - Location
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        let heading = newHeading.trueHeading

        if heading > 0 {
            // 1
        } else {
            // -1
        }

        // get delta
    }


    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        NSLog("location error")
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

    // MARK: - Actions
    @IBAction func stopAct(_ sender: UIButton) {

        angle+=0.5
        UIView.animate(withDuration: 0.5) {
            self.arrowView.transform = CGAffineTransform(rotationAngle: self.angle)
        }

    }

}
