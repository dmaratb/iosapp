//
//  DestinationsController.swift
//  FlashApp
//
//  Created by Marat on 05/11/2020.
//

import UIKit

class DestinationsController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Views
    @IBOutlet weak var destinationsTable: UITableView!
    
    // MARK: - Data
    var destinationsList:[Destination] = []
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if destinationsList.count > 0 {
            destinationsTable.reloadData()
        } else {
            // show flash
            let flashController:FlashController = storyboard?.instantiateViewController(withIdentifier: "FlashController") as! FlashController
            flashController.flashCallback = {value in
                self.destinationsList = value
            }
            navigationController?.pushViewController(flashController, animated: true)
        }
    }
    
    // MARK: - Tableview datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return destinationsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DestinationCell = tableView.dequeueReusableCell(withIdentifier: "DestinationCell", for: indexPath) as! DestinationCell

        let cellData = destinationsList[indexPath.row]

        cell.label.text = cellData.name
        return cell
    }
    
    
    // MARK: - Tableview delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // show flash
        let navController:NavigationController = storyboard?.instantiateViewController(withIdentifier: "NavigationController") as! NavigationController
        navController.destination = destinationsList[indexPath.row]
        navigationController?.pushViewController(navController, animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
